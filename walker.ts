import * as fs from "fs"
import * as path from "path"
import * as Promise from "bluebird"
import * as _ from "lodash"


function filewalker(dir, done) {
    let results = [];

    fs.readdir(dir, function (err, list) {
        if (err) return done(err);

        var pending = list.length;

        if (!pending) return done(null, results);

        list.forEach(function (file) {
            file = path.resolve(dir, file);

            fs.stat(file, function (err, stat) {
                // If directory, execute a recursive call
                if (stat && stat.isDirectory()) {
                    // Add directory to array [comment if you need to remove the directories from the array]
                    // results.push(file);

                    filewalker(file, function (err, res) {
                        results = results.concat(res);
                        if (!--pending) done(null, results);
                    });
                } else {
                    results.push(file);

                    if (!--pending) done(null, results);
                }
            });
        });
    });
};


export function walk(path:string) {
    return new Promise<string[]>((resolve, reject) => {

    filewalker(path, function (err, data) {
            if (err) return reject(err)

            resolve(data);
        })
    });
}