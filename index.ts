import * as walker from "./walker"
import * as _ from "lodash"
import * as writeFile from "write"
import * as fs from "fs-extra"
import * as async from "async"
import * as path from "path"
import { spawn } from "child_process"


interface IFileArray {
  index: number,
  file: string,
  prefix: string
}


export function conc(dir: string, options?: { ext?: string, outputFile?: string }) {
  return new Promise((resolve, reject) => {
    if (!options) options = {}
    if (!options.ext) options.ext = 'flv'
    walker.walk(dir).then((a) => {

      const reordered: IFileArray[] = []

      _.map(a, (seq) => {
        if (seq.split('.')[seq.split('.').length - 1] === options.ext && seq.split('_0').length > 1) {
          const seqIndexed = { file: seq, index: parseInt(seq.split('_0')[seq.split('_0').length - 1].split('_')[0].split('.')[0]), prefix: seq.split('/')[seq.split('/').length - 1].split('_0')[0] }
          reordered.push(seqIndexed)
        }
      })

      const uniq = _.uniqBy(reordered, 'prefix');

      const arrayOfUniquesArray: { prefix: string, array: IFileArray[] }[] = []

      _.map(reordered, (seq) => {
        let exists = false
        _.map(arrayOfUniquesArray, (un) => {
          if (un.prefix === seq.prefix) {
            un.array.push(seq)
            exists = true
          }
        })
        if (!exists) arrayOfUniquesArray.push({ prefix: seq.prefix, array: [seq] })
      })


      const rearr: { sequence: number, prefix: string, array: string[] }[] = []


      _.map(arrayOfUniquesArray, (un) => {
        un.array = _.orderBy(un.array, ['index'], ['asc'])
        let rangediff = un.array[0].index
        let lastprefix = un.array[0].prefix
        let index = 0
        rearr.push({ sequence: rangediff, prefix: lastprefix, array: [] })

        _.map(un.array, (seq) => {

          if (seq.index !== index + rangediff) {
            console.log(index + rangediff, seq.prefix, seq.index, '00')

            rangediff = 0
            lastprefix = seq.prefix
            index = seq.index
            console.log(index + rangediff, seq.prefix, seq.index, '0')

            rearr.push({ sequence: rangediff, prefix: seq.prefix, array: [seq.file] })
          } else {
            console.log(index + rangediff, seq.prefix, seq.index)

            _.map(rearr, (re) => {
              if (re.sequence === rangediff && re.prefix === seq.prefix) {
                re.array.push(seq.file)
              }
            })
          }
          index += 1
        })
      })


      async.eachSeries(rearr, (a, cb) => {
        let fileWirte = ''
        _.map(a.array, (seq, index) => {
          fileWirte += 'file \'' + seq + '\'\n'
        })

        const sequence = a.sequence

        const concatFile = '/tmp/concatenation.txt'
  
        let outputFile = dir+'/'+a.prefix + '_'+sequence+'_concatenated.' + options.ext

        if (options.outputFile) outputFile = options.outputFile
        fs.ensureDirSync(path.dirname(options.outputFile))
        writeFile.promise(concatFile, fileWirte).then(function () {

          const ffmpegConcat = spawn('ffmpeg', ['-f', 'concat', '-safe', '0', '-i', concatFile, '-c', 'copy', outputFile], {
            stdio: 'ignore',
            shell: true
          })

          ffmpegConcat.on('exit', function (code, signal) {
            fs.removeSync(concatFile)
            cb()
          });

          ffmpegConcat.on('error', function (code, signal) {
            fs.removeSync(concatFile)
            cb(code)
          });

        }).catch((err) => {
          fs.removeSync(concatFile)
          cb(err)
        })

      }, (err) => {
        if (err) return reject(err)
        resolve(true)
      })

    }).catch((err) => {
      reject(err)
    })
  })

}

