#!/usr/bin/env node
 
/**
 * Module dependencies.
 */

 var program = require('commander');

var concatenateapp=require('../index')

var version=require('../package.json').version

program

  .version(version, '-v, --version')
  .arguments('<dir>')
  .action(function(dir){
 if(dir){
return concatenateapp.conc(dir).then(()=>{
    console.log('done')
}).catch((err)=>{
    console.log(err)
})
 } else {
     console.error('no dir specified')
 }

  })
  .parse(process.argv)


