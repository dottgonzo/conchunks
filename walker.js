Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const path = require("path");
const Promise = require("bluebird");
function filewalker(dir, done) {
    let results = [];
    fs.readdir(dir, function (err, list) {
        if (err)
            return done(err);
        var pending = list.length;
        if (!pending)
            return done(null, results);
        list.forEach(function (file) {
            file = path.resolve(dir, file);
            fs.stat(file, function (err, stat) {
                if (stat && stat.isDirectory()) {
                    filewalker(file, function (err, res) {
                        results = results.concat(res);
                        if (!--pending)
                            done(null, results);
                    });
                }
                else {
                    results.push(file);
                    if (!--pending)
                        done(null, results);
                }
            });
        });
    });
}
;
function walk(path) {
    return new Promise((resolve, reject) => {
        filewalker(path, function (err, data) {
            if (err)
                return reject(err);
            resolve(data);
        });
    });
}
exports.walk = walk;
